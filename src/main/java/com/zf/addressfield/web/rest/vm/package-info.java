/**
 * View Models used by Spring MVC REST controllers.
 */
package com.zf.addressfield.web.rest.vm;
