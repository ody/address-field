import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IContact {
  id?: number;
  firstName?: string;
  lastName?: string;
  address?: string;
  phoneNumber?: number;
  email?: string;
  dob?: Moment;
  user?: IUser;
}

export class Contact implements IContact {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public address?: string,
    public phoneNumber?: number,
    public email?: string,
    public dob?: Moment,
    public user?: IUser
  ) {}
}
