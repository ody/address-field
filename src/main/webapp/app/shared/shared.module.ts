import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AddressfieldSharedCommonModule, HasAnyAuthorityDirective, JhiLoginModalComponent } from './';

@NgModule({
  imports: [AddressfieldSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [AddressfieldSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddressfieldSharedModule {
  static forRoot() {
    return {
      ngModule: AddressfieldSharedModule
    };
  }
}
